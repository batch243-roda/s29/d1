/**
 * [Section] comparison Query Operators
 *
 * $gt/$gte operator
 * - it allow us to find document that have field number value greater than or equal to a specified value
 * - it can be used by other methods like delete or update
 * - Syntax:
 *    db.collectionName.find({field: {$gt: value}});
 *    db.collectionName.find({field: {$gte: value}});
 */
// $gt
db.users.find({ age: { $gt: 50 } }).pretty();
// &gte
db.users.find({ age: { $gte: 21 } }).pretty();

/**
 * $lt/$lte operator
 * - it allow us to find document that have field number value less than or equal to a specified value
 *  - it can be used by other methods like delete or update
 * - Syntax:
 *    db.collectionName.find({field: {$lt: value}});
 *    db.collectionName.find({field: {$lte: value}});
 */
db.users.find({ age: { $lt: 50 } }).pretty();
db.users.find({ age: { $lte: 76 } }).pretty();

/**
 * $ne operator
 * - allows us to find documents that have field number values not equal to a specified value
 * Syntax:
 *    db.collectionName.find({field:{$ne:value}})
 */
db.users.find({ age: { $ne: 68 } }).pretty();

// $in operator
/**
 * -it allows us to find document/s with specific match criteria one field using different values.
 * Syntax:
 *    db.collectionName.find({field: {$in: [valueA, valueB]}})
 */
db.users.find({ lastName: { $in: ["Doe", "Hawking"] } }).pretty();

db.users.find({ courses: { $in: ["HTML", "React"] } }).pretty();

db.users.find({ courses: { $in: ["CSS", "JavaScript"] } }).pretty();

db.users.find({ age: { $gte: 18 } }).pretty();

/**
 * [Section] logical query operators
 * $or operator
 * - allows us to find documents that match a single criteria from multiple provided search criteria.
 * Syntax:
 *    db.collectionName.find({$or: [{fieldA: valueA}, {fieldB: valueB}]})
 */
db.users.find({ $or: [{ firstName: "Neil" }, { age: 25 }] }).pretty();

db.users.find({ $or: [{ firstName: "Neil" }, { age: { $gt: 30 } }] }).pretty();

/**
 * $and operator
 * - allows us to find documents that matching multiple criteria.
 * Syntax:
 *    db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}]})
 */
db.users.find({ $and: [{ firstName: "Neil" }, { age: { $gt: 30 } }] }).pretty();

db.users.find({ $and: [{ firstName: "Neil" }, { age: 30 }] }).pretty();

// [Section] Field projection
/**
 * - retrieving documents are common operations that we do and by default mongoDB query return the whole document as a response.
 */

// inclusion
/**
 * - it allows us to includ/add specific yields only when retrieving documents.
 * Syntax:
 *    db.users.find({criteria}, {fields: 1})
 */

db.users.find({ firstName: "Jane" }, { firstName: 1, lastName: 1, age: 1 });

db.users
  .find(
    { $and: [{ firstName: "Neil" }, { age: { $gt: 30 } }] },
    { firstName: 1, lastName: 1, age: 1 }
  )
  .pretty();

// Exclusion
/**
 * - allows us to exclude/remove specific fields only when retrieving documents.
 * - The value provided is 0.
 */

db.users.find(
  { firstName: "Jane" },
  {
    contact: 0,
    department: 0,
  }
);

db.users
  .find(
    { $and: [{ firstName: "Neil" }, { age: { $gt: 30 } }] },
    { age: 0, courses: 0 }
  )
  .pretty();

// Returning Specific fields in Embedded documents
db.users.find(
  { firstName: "Jane" },
  {
    "contact.phone": 1,
  }
);
// exclude ID
db.users.find(
  { firstName: "Jane" },
  {
    "contact.phone": 1,
    _id: 0,
  }
);

// [Section] Evaultion Query Operators
/**
 * - it allows us to find documents that match a specific string pattern using regular expressions.
 * Syntax:
 *    db.users.find({field: $regex: "pattern", $options: "$optionValue"})
 */

// case sensitive query
db.users.find({ firstName: { $regex: "N" } });
// case insensitive query
db.users.find({ firstName: { $regex: "ae", $options: "i" } });

// CRUD opertaions
db.users.updateOne(
  { age: { $lte: 17 } },
  {
    $set: {
      firstName: "Jayson",
      lastName: "Roda",
    },
  }
);

db.users.deleteOne({ $and: [{ firstName: "Jayson" }, { lastName: "Roda" }] });

db.users.find({ $and: [{ age: { $lte: 82, $gte: 76 } }] });
